//***************************************************************************
//                                                                          *
// SPKRID, A Prototype Text-Dependent Speaker Identification System.        *
// Copyright (C) 2002 A.Sankaranarayanan.                                   *
//                                                                          *
// This file is part of SPKRID.                                             *
//                                                                          *
// SPKRID is free software; you can redistribute it and/or                  *
// modify it under the terms of the GNU General Public License              *
// as published by the Free Software Foundation; either version 2           *
// of the License, or (at your option) any later version.                   *
//                                                                          *
// This program is distributed in the hope that it will be useful,          *
// but WITHOUT ANY WARRANTY; without even the implied warranty of           *
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
// GNU General Public License for more details.                             *
//                                                                          *
// You should have received a copy of the GNU General Public License        *
// along with this program; if not, write to the Free Software              *
// Foundation, Inc., 59 Temple Place - Suite 330, Boston,                   *
// MA  02111-1307, USA.                                                     *
//                                                                          *
// My e-mail address is: total_nerd@hotmail.com.                            *
//                                                                          *
//                                                                          *
// KEYCODE.HPP                                                              *
//                                                                          *
// Header file containing constants for various keys.                       *
// User interface component of the Speaker Identification Project.          *
//                                                                          *
// Last modified on March 16, 2002.                                         *
//                                                                          *
//***************************************************************************

#ifndef __KEYCODE_HPP
#define __KEYCODE_HPP

#define    KEY_UP_ARROW    0x4800
#define    KEY_DN_ARROW    0x5000
#define    KEY_LT_ARROW    0x4B00
#define    KEY_RT_ARROW    0x4D00
#define    KEY_ENTER       0x000D
#define    KEY_ESCAPE      0x001B
#define    KEY_BACKSPACE   0x0008
#define    KEY_DELETE      0x5300
#define    KEY_TAB         0x0009
#define    KEY_CTRL_TAB    0x9400
#define    KEY_HOME        0x4700
#define    KEY_END         0x4F00

#endif

//****************************** KEYCODE.HPP ********************************
